import sublime, sublime_plugin, os, subprocess, webbrowser, time, platform, sys
from threading import Thread

try:
    from Queue import Queue, Empty
except ImportError:
    from queue import Queue, Empty  # python 3.x

global deployResult

class DevaticsDeployCommand(sublime_plugin.WindowCommand):
    def run(self, server):
        # thread callback
        # self.output_view.run_command( 'devatics_write' )
        def enqueue_output(out, queue):
            global deployResult
            for line in iter(out.readline, b''):
                queue.put(line)
                text = line.decode()
                deployResult += text
                self.output_view.run_command( 'devatics_write' )
            out.close()

        global deployResult
        deployResult = ''

        # get file properties
        filename = self.window.active_view().file_name()

        # create output panel
        if not hasattr(self, 'output_view'):
            if int(sublime.version()) >= 3000:
                self.output_view = self.window.create_output_panel( 'devatics' )
            else:
                self.output_view = self.window.get_output_panel( 'devatics' )

        sublime.active_window().run_command("show_panel", {"panel" : "output.devatics" })
        self.output_view.settings().set("gutter", False)
        self.output_view.settings().set("scroll_past_end", False)
        self.output_view.settings().set("line_numbers", False)
        self.output_view.settings().set("scroll_speed",1)

        # if file is correct
        if filename.find( 'config.js' ) != -1:
            if platform.system() == 'Windows':
                folders = filename.split( '\\' )
                folder = folders[ len(folders) - 2 ]
                client = folder.split( '-' )[0]
                folders.pop()
                path = '\\'.join( folders )

                command = "cd " + path + ";"
                command += "scp config.js devatics@" + server + ".devatics.com:/home/devatics/sites/" + client + "/config.js;"
                command += "ssh devatics@" + server + ".devatics.com 'cd /home/devatics/newplatform/tools/deploy/ && ./jsbuilder.sh -c " + client + "';"
                command += "if(Get-Command New-BurntToastNotification --errorAction SilentContinue){New-BurntToastNotification -FirstLine 'Deploy' -SecondLine '" + client + " has been deployed on " + server + "'};"

                process = subprocess.Popen([r'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe',
                     '-ExecutionPolicy',
                     'Unrestricted',
                     command ], cwd='C:\\', stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1, shell=True)
            else:
                folders = filename.split('/')
                client = folders[ len(folders) - 2 ]
                command = 'scp ' + filename + ' devatics@' + server + '.devatics.com:/home/devatics/sites/' + client + '/config.js'
                command += ' && ssh devatics@' + server + '.devatics.com "cd /home/devatics/newplatform/tools/deploy/ && ./jsbuilder.sh -c ' + client + '"'
                command += ' && notify-send -i sublime-text -u low -t 2000 "' + client + ' has been deployed on ' + server + '"'
                process = subprocess.Popen( command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1, executable='/bin/bash')

            q = Queue()
            t = Thread( target=enqueue_output, args=(process.stdout, q))
            t.daemon = True
            t.start()
            t2 = Thread( target=enqueue_output, args=(process.stderr, q))
            t2.daemon = True
            t2.start()

            try: line = q.get_nowait()
            except Empty :
                deployResult += 'Transfering file ...\n'
                self.output_view.run_command( 'devatics_write' )
        else :
            deployResult += '[30m---ERROR : Not a premium file\n'
            self.output_view.run_command( 'devatics_write' )

class DevaticsWriteCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        global deployResult
        view = self.view
        view.set_read_only( False )
        region = sublime.Region(0, view.size())
        view.erase(edit, region)
        view.insert(edit, 0, deployResult)

        # apply ansi
        view.settings().set('ansi_enabled', False)
        if int(sublime.version()) >= 3000:
            view.run_command("ansi")
        # scroll to the bottom
        view.show(view.size())

class DevaticsShowCommand(sublime_plugin.WindowCommand):
    def run(self):
        windows = sublime.windows()
        active = False
        for window in windows:
            if int(sublime.version()) >= 3000:
                if window.active_panel() == 'output.devatics':
                    active = True
        if active:
            sublime.active_window().run_command("hide_panel", {"panel" : "output.devatics"})
        else:
            sublime.active_window().run_command("show_panel", {"panel" : "output.devatics"})

class DevaticsJiraCommandLine(sublime_plugin.WindowCommand):
    def run(self, text):
        if not self.window.active_view(): return
        view = self.window.active_view()
        selection = False
        for region in view.sel():
            if not region.empty() and not selection:
                selection = True
                self.on_done( view.substr(region))
        if not selection:
            v = self.window.show_input_panel('Enter a JIRA Ticket number', text, self.on_done, None, None)
    def on_done(self, text):
        if text != '':
            webbrowser.open( 'https://campaigns.devatics.com/browse/' + text )

class DevaticsBuildCommand(sublime_plugin.WindowCommand):
    def run(self, dev=False):
        def enqueue_output(out, queue):
            global deployResult
            for line in iter(out.readline, b''):
                queue.put(line)
                text = line.decode()
                deployResult += text
                self.output_view.run_command( 'devatics_write' )
            out.close()

        global deployResult
        deployResult = ''

        filename = self.window.active_view().file_name()
        # create output panel
        if not hasattr(self, 'output_view'):
            if int(sublime.version()) >= 3000:
                self.output_view = self.window.create_output_panel( 'devatics' )
            else:
                self.output_view = self.window.get_output_panel( 'devatics' )

        sublime.active_window().run_command("show_panel", {"panel" : "output.devatics" })
        self.output_view.settings().set("gutter", False)
        self.output_view.settings().set("scroll_past_end", False)
        self.output_view.settings().set("line_numbers", False)
        self.output_view.settings().set("scroll_speed",1)

        if platform.system() == 'Windows':
            folders = filename.split( '\\' )
            repoIndex = folders.index('repo')
            client = folders[repoIndex + 1]
            subfolder = folders[:repoIndex]
            path = '\\'.join(subfolder)

            command = "cd " + path +";"
            # command += "node app.js build " + client + ';'
            command += "node app.js build " + ( "-d " if dev else "" ) + client + ';'
            command += "if(Get-Command New-BurntToastNotification --errorAction SilentContinue){New-BurntToastNotification -FirstLine 'Build' -SecondLine '" + client + " has been built'};"

            process = subprocess.Popen([r'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe',
                 '-ExecutionPolicy',
                 'Unrestricted',
                 command ], cwd='C:\\', stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1, shell=True)
        else:
            folders = filename.split('/')
            repoIndex = folders.index('repo')
            client = folders[repoIndex + 1]
            subfolder = folders[:repoIndex]
            path = '/'.join( subfolder )
            command = "cd " + path
            command += " && node app.js build " + client
            process = subprocess.Popen( command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1, executable='/bin/bash')

        q = Queue()
        t = Thread( target=enqueue_output, args=(process.stdout, q))
        t.daemon = True
        t.start()
        t2 = Thread( target=enqueue_output, args=(process.stderr, q))
        t2.daemon = True
        t2.start()

        try: line = q.get_nowait()
        except Empty :
            deployResult += 'Building ' + client
            self.output_view.run_command( 'devatics_write' )