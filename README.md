## devatics-sublime
Ce package est à destination de l'environnement de travail des CSD au sein de l'entreprise devatics

Il comprend :

* script de déploiement sur les serveurs de test. Script compatible sous linux et sous windows sous réserve de la création de la commande deploy pour powershell. Raccourcis : F1 (zidane) et F3 (zizou).

* accès à un ticket jira. Raccourci F2

* Syntaxe javascript personnalisée pour les fichiers config.js

* Des snippets pour permettre la création d'objets AMD (action, callers, behaviours)

## Windows

Utiliser : https://github.com/Windos/BurntToast/blob/master/BurntToast.png pour afficher une notification en fin de déploiement